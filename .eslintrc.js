module.exports = {
    env: {
        commonjs: true,
        es6: true,
        node: true,
    },
    extends: ["airbnb-base", "prettier"],
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly",
    },
    parserOptions: {
        ecmaVersion: 2018,
    },
    rules: {
        "linebreak-style": 0,
        "operator-linebreak": ["error", "after"],
    },
};
