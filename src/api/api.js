const Router = require("koa-router");

const koaBody = require("koa-body");
const log = require("../utils/logger");
const { version } = require("../../package.json");
const { startApp } = require("../services/appManager.js");
const { sendTgMsg, deploy } = require("../services/deploy.js");
const {
    CMD_DEPLOY_SWARM,
    CMD_DEPLOY_STATIC,
} = require("../constants/commandsConsts");
const { successResponse } = require("../utils/helpers");

const mainApi = new Router({ prefix: "/api" });

/**
 * Sending message to telegram from Adoobot
 * @example POST: adooone.space/api/deploy_stack?
 */
mainApi.post("/deploy_stack", koaBody(), async (ctx, next) => {
    log.info("Deploying stack...");
    deploy(CMD_DEPLOY_SWARM);
    await next();

    // response
    ctx.body = successResponse("Deploying in progress...");
});

/**
 * Deploying static files
 * @example POST: adooone.space/api/deploy_static?
 */
mainApi.post("/deploy_static", koaBody(), async (ctx, next) => {
    log.info("Deploying static...");
    const {
        request: { body },
    } = ctx;
    deploy(CMD_DEPLOY_STATIC, {
        SRC: body.src,
        NAME: body.name,
    });
    await next();

    // response
    ctx.body = successResponse("Deploying in progress...");
});

/**
 * Sending message to telegram from Adoobot
 * @example POST: adooone.space/api/sendmsg?
 */
mainApi.post("/sendmsg", koaBody(), async (ctx, next) => {
    const {
        request: { body },
    } = ctx;

    const response = await sendTgMsg(body.msg);
    await next();

    // response
    ctx.body = response;
});

/**
 * Ping request with version response
 * @example POST: adooone.space/core/ping?
 */
mainApi.get("/ping", async (ctx, next) => {
    log.info(version);

    const response = { msg: "pong", core: { version } };
    await next();

    // response
    ctx.body = response;
});

/**
 * Ping request with version response
 * @example POST: adooone.com/core/start?
 */
mainApi.get("/start", async (ctx, next) => {
    log.info(version);

    const response = { msg: "started" };
    await startApp(ctx);
    await next();

    // response
    ctx.body = response;
});

module.exports = mainApi;
