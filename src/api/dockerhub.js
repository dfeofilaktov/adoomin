const Router = require("koa-router");

const koaBody = require("koa-body");
const log = require("../utils/logger");
const { sendTgMsg } = require("../services/deploy");
const { deploy } = require("../services/deploy");
const { CMD_DEPLOY_SWARM } = require("../constants/commandsConsts");

const dockercb = new Router({ prefix: "/dockerhub" });

/**
 * DockerHub hooks serve
 * @example POST: adooone.space/dockerhub/hook?
 */
dockercb.post("/hook", koaBody(), async (ctx, next) => {
    log.info('Hook from DockerHub was recieved!');
    const { request: { body } } = ctx;
    const {
        push_data: pushed,
        repository,
    } = body;

    const data = {
        name: repository.repo_name,
        tag: pushed.tag,
    }

    log.info(`Image ${data.name}:${data.tag} was pushed to DockerHub.`);
    sendTgMsg(`Image ${data.name}:${data.tag} was pushed to DockerHub.`);
    deploy(CMD_DEPLOY_SWARM);

    await next();

    // response
    ctx.body = 'thanks';
});

module.exports = dockercb;
