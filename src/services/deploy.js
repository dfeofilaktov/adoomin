const axios = require("axios");
const { sendCommand } = require("../utils/commands");
const { successResponse, errorResponse } = require("../utils/helpers");
// const { CMD_SEND_MSG } = require("../constants/commandsConsts");
const log = require("../utils/logger");

async function sendTgMsg(msg) {
    log.info("Sending message");
    try {
        await axios.post(
            "https://api.telegram.org/bot1066429429:AAF-BV7rFSXa9Z88wxGtr13aKmXTc5wM-lk/sendMessage?",
            {
                text: msg,
                chat_id: "-351510806",
                // parse_mode: "MarkdownV2",
            }
        );
    } catch (e) {
        log.error(e.message);
        return errorResponse(`Error while sending message: ${e.message}`)
    }
    // await sendCommand(CMD_SEND_MSG, null, { env: { MSG: msg } });
    return successResponse("Message was sent");
}

async function deploy(cmd, params) {
    log.info(`Deploying ${cmd}`);
    try {
        await sendCommand(cmd, null, { env: params });
        // await sendTgMsg(`${cmd.replace("_", " ")} was done`);
    } catch (e) {
        await sendTgMsg(`${cmd.replace("_", " ")} was not done. Error: ${e}`);
    }
}

module.exports = {
    deploy,
    sendTgMsg,
};
