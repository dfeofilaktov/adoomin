const log = require("../utils/logger");

const startApp = async function startApp(/* ctx */) {
    log.debug("In startApp!");
};

module.exports = {
    startApp,
};
