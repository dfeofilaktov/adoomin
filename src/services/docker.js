// const path = require('path');
const Docker = require('dockerode');
const appConfig = require('../app.config');

const docker = new Docker(appConfig.docker);

const getInfo = async function getInfo() {
    const res = await docker.info();
    return {
        Containers: res.Containers,
        ContainersRunning: res.ContainersRunning,
        ContainersPaused: res.ContainersPaused,
        ContainersStopped: res.ContainersStopped,
        Images: res.Images,
        SystemTime: res.SystemTime,
        // Swarm: res.Swarm,
    };
}

const buildImage = async function buildImage(tag, context, files = ['Dockerfile']) {
    try {
        await docker.buildImage(
            { src: files, context },
            { t: tag })
        ;;
        const res = await docker.listImages();
        return res;
    } catch(e) {
        return e;
    }
}

module.exports = {
    getInfo,
    buildImage,
}
