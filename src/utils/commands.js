const path = require("path");
const util = require("util");
const execFileProcess = require("child_process").execFile;
const log = require("./logger");

const execFile = util.promisify(execFileProcess);

async function sendCommand(command, args = [], options = {}) {
    log.info(`Sending command ${command}`)
    try {
        // await execFile("chmod", [
        //     "+rx",
        //     path.resolve(__dirname, `../commands/${command}.sh`),
        // ]);
        const { stdout } = await execFile(
            path.resolve(__dirname, `../commands/${command}.sh`),
            args,
            { shell: '/bin/fish', ...options }
        );
        log.info(stdout);
        return true;
    } catch(e) {
        log.error(e.stderr);
        throw new Error(e.stderr);
    }
}

module.exports = {
    sendCommand,
};