const { createLogger, format, transports } = require("winston");

const { combine, timestamp, prettyPrint, printf } = format;

const myFormat = printf((text) => {
    return `${text.timestamp} ${text.level}: ${text.message}`;
});

const log = createLogger({
    level: "info",
    format: combine(
        timestamp({ format: "DD-MM-YYYY HH:mm:ss" }),
        prettyPrint(),
        myFormat
    ),
    transports: [
        new transports.File({
            filename: "./logs/error.log",
            level: "error",
            maxsize: "10000000",
            maxFiles: "10",
        }),
        new transports.File({
            filename: "./logs/combined.log",
            maxsize: "10000000",
            maxFiles: "10",
        }),
    ],
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== "production") {
    log.add(
        new transports.Console({
            format: combine(
                timestamp({ format: "HH:mm:ss" }),
                prettyPrint(),
                myFormat
            ),
        })
    );
}

module.exports = log;
