module.exports = {
    "app": {
        "host": process.env.APP_HOST || "0.0.0.0",
        "port": process.env.APP_PORT || 8633,
        "log_level": process.env.LOG_LEVEL || 'error'
    },
    "docker": {
        "host": "127.0.0.1",
        "port": 4243,
    }
}