#!/bin/bash
cd ~/adoomin
mkdir .tmp
cd .tmp

src=${SRC}
name=${NAME}

# src='https://dfeofilaktov@bitbucket.org/dfeofilaktov/frontend.git'
# name='frontend'

echo '*** Getting static' $name'...'
git clone $src
cd $name

echo '*** Building' $name'...'
npm rm -rf package-lock.json
npm i
npm run build

echo '*** Copying files...'
cd ~/adoomin/stack/static
rm -rf $name
cp -r ~/adoomin/.tmp/$name/dist ./$name
rm -rf ~/adoomin/.tmp

echo 'Done!'