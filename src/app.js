const dotenv = require("dotenv");
const http = require("http");
const Koa = require("koa");
const helmet = require("koa-helmet");
const compress = require("koa-compress");

const { startApp } = require("./services/appManager.js");
const { listeningReporter } = require("./utils/helpers");

const log = require("./utils/logger");
const api = require("./api/api.js");
const dockerhubApi = require("./api/dockerhub.js");

const config = require('./app.config'); 

dotenv.config();

const app = new Koa();

app.use(helmet());
app.use(compress());
app.use(api.routes()).use(api.allowedMethods());
app.use(dockerhubApi.routes()).use(dockerhubApi.allowedMethods());

(async function () {
    http.createServer(app.callback()).listen(
        config.app.port,
        config.app.host,
        listeningReporter
    );
    log.info("App is starting");
    await startApp(app.context);
})();
